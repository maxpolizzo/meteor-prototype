import React from 'react';
import PropTypes from 'prop-types';

import Mobile from './components/mobile/main'

import DesktopHeader from './components/desktop/header/header'
import DesktopMain from './components/desktop/main/main'

class App extends React.Component {


  constructor(props){
    super(props);
    this.state = {
      width: window.innerWidth,
    };
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  // make sure to remove the listener when the component is not mounted anymore
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  render(){

    console.log('render App');

    const { width } = this.state;
    const isMobile = width <= 500;

    console.log(width)

    if(isMobile){
      return(

        <div>
          <Mobile screenWidth={ width }/>
        </div>

      )
    } else {
      return(

        <div>
          <DesktopHeader/>
          <DesktopMain deviceType='desktop'/>
        </div>

      )
    }



  }

}

App.propTypes = {

};

export default App;
